const express = require('express');
const app = express();
const axios = require('axios');
var bodyParser = require('body-parser');
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))


app.get('/users', async function (req, res) {
    try{
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users')
        res.send(data)
    }catch(error){
        res.send(error)
    }
})

app.get('/users/:id', async function (req, res) {
    try{
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+req.params.id)
        res.send(data)
    }catch(error){
        res.send(error)
    }
})

app.post('/users',async function (req, res) {
    try{
        const {data} = await axios.post('https://jsonplaceholder.typicode.com/users', {
            name : 'Thomas Morgana',
            username : 'Bateau',
            email : 'thomas.morgana@ynov.com',
            phone: '06345684512',})
        res.send(data)
    }catch(error){
        res.send(error)
    }
})

app.put('/users/:id', async function (req, res) {
    try{
        const {data} = await axios.put('https://jsonplaceholder.typicode.com/users/'+req.params.id, req.query)
        res.send(data)
    }catch(error){
        res.send(error)
    }
})

app.delete('/users/:id', async function (req, res) {
    try{
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+req.params.id)
        res.send(data)
    }catch(error){
        res.send(error)
    }
})
 

app.get('/users/:id/posts',async function(req,res){
    try{
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+req.params.id+'/posts')
        res.send(data)
    }catch(error){
        res.send(error);
    }
})

app.get('/users/:id/albums',async function(req,res){
    try{
        const user = await axios.get(`https://jsonplaceholder.typicode.com/users/`+req.params.id)
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+req.params.id+'/albums')
        const dataPhoto = await axios.get('https://jsonplaceholder.typicode.com/photos')
        let i =0;
        while(data[i] != undefined){
            data[i]['photo']=dataPhoto.data;
            i++
        }        
        user.data['albums'] = final
        res.send(user)
    }catch(error){
        res.send(error);
    }
})
app.listen(3000)






//https://jsonplaceholder.typicode.com/